<?php

/**
 * @file
 * The Milo Project is an initiative to empower teachers to create digital
 * teaching resources using H5P.
 */

/**
 * Implements hook_menu().
 */
function milo_menu() {
  return array(
    'user/edit' => array(
      'access callback' => 'user_is_logged_in',
      'page callback' => 'milo_user_edit_redirect',
      'type' => MENU_CALLBACK,
    ),
    'login' => array(
      'access callback' => 'user_is_anonymous',
      'page callback' => 'milo_login_redirect',
      'type' => MENU_CALLBACK,
    ),
    'play/%' => array(
      'access callback' => TRUE,
      'page arguments' => array(1),
      'page callback' => 'milo_play_node',
      'type' => MENU_CALLBACK,
    ),
  );
}

/**
 * Implements hook_form_FORM_ID_alter() for user_login().
 */
function milo_form_user_login_alter(&$form, &$form_state) {
  // Check if it has a "destination" set. If so, and it matched the h5p_content
  // creation form, change the page title.
  if (
    !empty($_GET['destination']) &&
    preg_match('/node\/add\/h5p-content/', $_GET['destination'])
  ) {
    drupal_set_title(t("Please login first"));
  }
  else {
    drupal_set_title(t("Log in"));
  }

  // Add placeholders.
  $form['name']['#placeholder'] = t("Email");
  $form['name']['#type'] = 'emailfield';
  $form['pass']['#placeholder'] = t("Password");

  // Add a links to create a new account or reset the password.
  $form['account_links'] = array(
    '#prefix' => '<div class="user-form__links">',
    '#suffix' => '</div>',
  );

  $form['account_links']['account_creation_link'] = array(
    '#markup' => '<div class="user-form__links__create-account">' . t("Don't have an account yet? <a href='!link'>Create one now!</a>", array(
      '!link' => url('user/register'),
    )) . '</div>',
  );

  $form['account_links']['account_pass_reset_link'] = array(
    '#markup' => '<div class="user-form__links__reset-pass">' . t("<a href='!link'>Forgot your password?</a>", array(
      '!link' => url('user/password'),
    )) . '</div>',
  );
}

/**
 * Implements hook_form_FORM_ID_alter() for user_register_form().
 */
function milo_form_user_register_form_alter(&$form, $form_state) {
  // Change the page title.
  drupal_set_title(t("Create a new account"));

  // Add placeholders.
  // @todo Only put an email field; use it for the username.
  $form['account']['name']['#placeholder'] = t("Email (todo, remove)");
  $form['account']['name']['#type'] = 'emailfield';
  $form['account']['mail']['#placeholder'] = t("Email");
  $form['account']['mail']['#type'] = 'emailfield';

  // Add a links to login or reset the password.
  $form['account_links'] = array(
    '#prefix' => '<div class="user-form__links">',
    '#suffix' => '</div>',
  );

  $form['account_links']['account_login_link'] = array(
    '#markup' => '<div class="user-form__links__login">' . t("Already have an account? <a href='!link'>Log in here.</a>", array(
      '!link' => url('login', array(
        'query' => array(
          // @todo Go to "My content".
          'destination' => 'user',
        )
      )),
    )) . '</div>',
  );

  $form['account_links']['account_pass_reset_link'] = array(
    '#markup' => '<div class="user-form__links__reset-pass">' . t("<a href='!link'>Forgot your password?</a>", array(
      '!link' => url('user/password'),
    )) . '</div>',
  );
}


/**
 * Implements hook_form_FORM_ID_alter() for user_pass().
 */
function milo_form_user_pass_alter(&$form, $form_state) {
  // Change the page title.
  drupal_set_title(t("Reset your password"));

  // Add placeholder and use HTML5 type.
  $form['name']['#placeholder'] = t("Email");
  $form['name']['#type'] = 'emailfield';

  // Add a links to login or create a new account.
  $form['account_links'] = array(
    '#prefix' => '<div class="user-form__links">',
    '#suffix' => '</div>',
  );

  $form['account_links']['account_login_link'] = array(
    '#markup' => '<div class="user-form__links__login">' . t("Already have an account? <a href='!link'>Log in here.</a>", array(
      '!link' => url('login', array(
        'query' => array(
          // @todo Go to "My content".
          'destination' => 'user',
        )
      )),
    )) . '</div>',
  );

  $form['account_links']['account_creation_link'] = array(
    '#markup' => '<div class="user-form__links__create-account">' . t("Don't have an account yet? <a href='!link'>Create one now!</a>", array(
      '!link' => url('user/register'),
    )) . '</div>',
  );
}

/**
 * Implements hook_form_FORM_ID_alter() for user_profile_form().
 */
function milo_form_user_profile_form_alter(&$form, $form_state) {
  $form['account']['name']['#type'] = 'emailfield';
  $form['account']['mail']['#type'] = 'emailfield';
}

/**
 * Implements hook_menu_local_tasks_alter().
 */
function milo_menu_local_tasks_alter(&$data, $router_item, $root_path) {
  global $user;
  $current_item = menu_get_item();

  // Determine the context.
  $is_in_h5p_context = FALSE;
  $nid = NULL;
  if (!empty($current_item['page_arguments'])) {
    if (
      !empty($current_item['page_arguments'][0]->nid) &&
      $current_item['page_arguments'][0]->type == 'h5p_content'
    ) {
      $is_in_h5p_context = TRUE;
      $nid = $current_item['page_arguments'][0]->nid;
    }
    elseif (
      $current_item['path'] == 'node/%/h5p-results' &&
      !empty($current_item['page_arguments'][2]) &&
      is_numeric($current_item['page_arguments'][2])
    ) {
      $is_in_h5p_context = TRUE;
      $nid = $current_item['page_arguments'][2];
    }
  }

  if (!isset($data['tabs'][0])) {
    $data['tabs'][0] = array('count' => 0, 'output' => array());
  }

  // Add a "Create new content" tab.
  if (!empty($user->uid)) {
    $link = menu_get_item('node/add/h5p-content');
    $link['title'] = t("Create");
    $link['localized_options']['attributes']['class'][] = 'add-content';
    $data['tabs'][0]['count']++;
    $data['tabs'][0]['output'][] = array(
      '#theme' => 'menu_local_task',
      '#link' => $link,
      '#weight' => -100,
      '#active' => preg_match('/node\/add\/h5p-content/', $current_item['path']),
    );
  }
  else {
    $link = menu_get_item('user/login');
    $link['title'] = t("Create");
    // @todo Go to "My content".
    $link['localized_options']['query']['destination'] = 'user';
    $link['localized_options']['attributes']['class'][] = 'add-content';
    $data['tabs'][0]['count']++;
    $data['tabs'][0]['output'][] = array(
      '#theme' => 'menu_local_task',
      '#link' => $link,
      '#weight' => -100,
    );
  }

  foreach ($data['tabs'][0]['output'] as $i => &$tab) {
    if (preg_match('/node\/%\/view/', $tab['#link']['path'])) {
      // Rename the view tab to "Preview" and add an icon.
      $tab['#link']['localized_options']['attributes']['class'][] = 'preview-content';
      $tab['#link']['title'] = t("Preview");
    }
    elseif (preg_match('/node\/%\/edit/', $tab['#link']['path'])) {
      // Add a class to the edit tab.
      $tab['#link']['localized_options']['attributes']['class'][] = 'edit-content';
    }
    elseif (preg_match('/node\/%\/h5p-results/', $tab['#link']['path'])) {
      // Add a class to the results tab.
      $tab['#link']['localized_options']['attributes']['class'][] = 'view-results';
      $tab['#weight'] = 50;
    }
    elseif (preg_match('/node\/%\/revisions/', $tab['#link']['path'])) {
      // Add a class to the revisions tab.
      $tab['#link']['localized_options']['attributes']['class'][] = 'view-revisions';
    }
  }

  // Add a "play" action.
  if ($is_in_h5p_context) {
    $link = menu_get_item('play/' . $nid);
    $link['title'] = t("Play");
    $link['localized_options']['attributes']['class'][] = 'play-content';
    $link['localized_options']['attributes']['target'] = '_blank';
    $data['tabs'][0]['count']++;
    $data['tabs'][0]['output'][] = array(
      '#theme' => 'menu_local_task',
      '#link' => $link,
      '#weight' => 200,
    );
  }

  // If there's a "Clone content" action, move it to the tabs.
  if (!empty($data['actions']['output'])) {
    foreach ($data['actions']['output'] as $i => $action) {
      if (preg_match('/node\/%\/clone\/%/', $action['#link']['path'])) {
        $action['#link']['title'] = t("Copy");
        $action['#link']['localized_options']['attributes']['class'][] = 'clone-content';
        $action['#weight'] = -50;
        $data['tabs'][0]['count']++;
        $data['tabs'][0]['output'][] = $action;

        $data['actions']['count']--;
        unset($data['actions']['output'][$i]);
        break;
      }
    }
  }
  elseif ($is_in_h5p_context) {
    // If we are in context of an H5P node, and no action is available, we're
    // probably editing it. Add the tab here.
    $link = menu_get_item('node/' . $nid . '/clone/' . clone_get_token($nid));
    $link['title'] = t("Copy");
    $link['localized_options']['attributes']['class'][] = 'clone-content';
    $data['tabs'][0]['count']++;
    $data['tabs'][0]['output'][] = array(
      '#theme' => 'menu_local_task',
      '#link' => $link,
      '#weight' => -50,
    );
  }
}

/**
 * Implements hook_clone_node_alter().
 */
function milo_clone_node_alter(&$node, $context) {
  // h5p doesn't handle cloning. Handle cloning for it. See milo_node_insert().
  if ($context['original_node']->type == 'h5p_content') {
    $node->h5p_cloned_node = $context['original_node'];
  }
}

/**
 * Implements hook_node_insert().
 */
function milo_node_insert($node) {
  // When cloning, we cannot alter the cloned node with hook_clone_node_alter(),
  // as H5P requires a NID and VID for storage. Instead, we set a custom
  // property, and of present, perform the cloning.
  if (
    $node->type == 'h5p_content' &&
    isset($node->h5p_cloned_node) &&
    !isset($node->h5p_cloned)
  ) {
    // Copy old package.
    $h5p_storage = _h5p_get_instance('storage');
    $h5p_storage->copyPackage(
      $node->vid,
      $node->h5p_cloned_node->vid,
      $node->nid
    );

    $node->h5p_cloned = TRUE;

    // Insert content.
    h5p_insert($node);
  }
}

/**
 * Page callback: redirect to user edit form.
 */
function milo_user_edit_redirect() {
  global $user;
  drupal_goto("user/{$user->uid}/edit", array(
    'query' => array(
      'destination' => "user/{$user->uid}/edit",
    ),
  ));
}

/**
 * Page callback: redirect to user login form.
 */
function milo_login_redirect() {
  global $user;
  drupal_goto("user/login", array(
    'query' => array(
      'destination' => 'user',
    ),
  ));
}

/**
 * Page callback: show an H5P content.
 */
function milo_play_node($nid) {
  h5p_embed($nid);
  // @todo
  print "Some more stuff to embed";
}
